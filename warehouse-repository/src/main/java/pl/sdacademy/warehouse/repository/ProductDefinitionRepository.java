package pl.sdacademy.warehouse.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.sdacademy.warehouse.domain.entity.ProductDefinition;

@Repository
public interface ProductDefinitionRepository extends JpaRepository<ProductDefinition,Long> {
}
