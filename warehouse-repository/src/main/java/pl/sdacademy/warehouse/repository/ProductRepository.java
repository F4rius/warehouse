package pl.sdacademy.warehouse.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.sdacademy.warehouse.domain.entity.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {
}
