package pl.sdacademy.warehouse.domain.entity.utils;

public enum AccountType {

    WAREHOUSEMAN,
    ENGINEER,
    ADMIN,
    OTHER,
}
