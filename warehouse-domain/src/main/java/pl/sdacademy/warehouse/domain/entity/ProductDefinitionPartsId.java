package pl.sdacademy.warehouse.domain.entity;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;

@Embeddable
public class ProductDefinitionPartsId {

    @ManyToOne
    private ProductDefinition productDefinition;

    @ManyToOne
    private PartDefinition partDefinition;

    public ProductDefinition getProductDefinition() {
        return productDefinition;
    }

    public void setProductDefinition(ProductDefinition productDefinition) {
        this.productDefinition = productDefinition;
    }

    public PartDefinition getPartDefinition() {
        return partDefinition;
    }

    public void setPartDefinition(PartDefinition partDefinition) {
        this.partDefinition = partDefinition;
    }
}
