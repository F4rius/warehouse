package pl.sdacademy.warehouse.domain.entity;

import javax.persistence.*;

@Entity
@Table(name = "part")
public class Part {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="part_id")
    private Long id;

    @Column(name="quantity")
    private double quantity;

    @OneToOne(fetch = FetchType.LAZY,
            cascade =  CascadeType.ALL,
            mappedBy = "part")
    private PartDefinition partDefinition;

    private int version;

    public Part(){
    }

    public Part(double quantity, PartDefinition partDefinition) {
        this.partDefinition = partDefinition;
        this.quantity = quantity;
    }

    public PartDefinition getPartDefinition() {
        return partDefinition;
    }

    public void setPartDefinition(PartDefinition partDefinition) {
        this.partDefinition = partDefinition;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }
}

