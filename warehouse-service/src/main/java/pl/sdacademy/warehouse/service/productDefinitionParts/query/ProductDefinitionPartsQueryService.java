package pl.sdacademy.warehouse.service.productDefinitionParts.query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.sdacademy.warehouse.domain.entity.ProductDefinition;
import pl.sdacademy.warehouse.domain.entity.ProductDefinitionParts;
import pl.sdacademy.warehouse.domain.entity.ProductDefinitionPartsId;
import pl.sdacademy.warehouse.repository.PartDefinitionRepository;
import pl.sdacademy.warehouse.repository.ProductDefinitionPartsRepository;
import pl.sdacademy.warehouse.repository.ProductDefinitionRepository;
import pl.sdacademy.warehouse.service.productDefinitionParts.exception.ProductDefinitionPartsNotFoundException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional(readOnly = true)
public class ProductDefinitionPartsQueryService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProductDefinitionPartsQueryService.class);
    public static final String INITIAL_QUANTITY = "0";

    private final ProductDefinitionPartsRepository productDefinitionPartsRepository;
    private final PartDefinitionRepository partDefinitionRepository;
    private final ProductDefinitionRepository productDefinitionRepository;

    @Autowired
    public ProductDefinitionPartsQueryService(ProductDefinitionPartsRepository productDefinitionPartsRepository, PartDefinitionRepository partDefinitionRepository, ProductDefinitionRepository productDefinitionRepository) {
        this.productDefinitionPartsRepository = productDefinitionPartsRepository;
        this.partDefinitionRepository = partDefinitionRepository;
        this.productDefinitionRepository = productDefinitionRepository;
    }

    public List<ProductDefinitionParts> findAll(Long productDefinitionId) {
        ProductDefinition productDefinition = productDefinitionRepository.findOne(productDefinitionId);

        List<ProductDefinitionParts> productDefinitionsParts = partDefinitionRepository.findAll().stream()
                .map(partDefinition -> {
                    ProductDefinitionPartsId productDefinitionPartsId = new ProductDefinitionPartsId();
                    productDefinitionPartsId.setPartDefinition(partDefinition);
                    productDefinitionPartsId.setProductDefinition(productDefinition);

                    ProductDefinitionParts productDefinitionParts =
                            productDefinitionPartsRepository.findByProductDefinitionPartsId(productDefinitionPartsId);

                    return Optional.ofNullable(productDefinitionParts)
                            .orElse(new ProductDefinitionParts(productDefinitionPartsId, INITIAL_QUANTITY));
                }).collect(Collectors.toList());

        return productDefinitionsParts;
    }

    public ProductDefinitionParts findById(Long id) {
        ProductDefinitionParts productDefinitionParts = productDefinitionPartsRepository.findOne(id);
        if (productDefinitionParts == null) {
            LOGGER.debug("Part with id " + id + " not found.");
            throw new ProductDefinitionPartsNotFoundException();
        }

        return productDefinitionParts;
    }

    public ProductDefinitionParts findByProductDefinitionPartsId (ProductDefinitionPartsId productDefinitionPartsId){
        ProductDefinitionParts productDefinitionParts = productDefinitionPartsRepository.findByProductDefinitionPartsId(productDefinitionPartsId);
        return productDefinitionParts;
    }
}
