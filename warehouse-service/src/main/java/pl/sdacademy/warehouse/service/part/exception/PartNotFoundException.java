package pl.sdacademy.warehouse.service.part.exception;

public class PartNotFoundException extends RuntimeException {
    private static final long serialVersionUID = 1369368614263620977L;
}
