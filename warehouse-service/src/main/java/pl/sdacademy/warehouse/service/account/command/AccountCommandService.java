package pl.sdacademy.warehouse.service.account.command;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.sdacademy.warehouse.domain.entity.Account;
import pl.sdacademy.warehouse.repository.AccountRepository;
import pl.sdacademy.warehouse.service.account.exception.AccountNotFoundException;

@Service
@Transactional
public class AccountCommandService {


    private final Logger LOGGER = LoggerFactory.getLogger(AccountCommandService.class);

    private AccountRepository accountRepository;

    @Autowired
    public AccountCommandService(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public Long saveAccount(Account account) {
        accountRepository.save(account);

        return account.getId();
    }

    public void update(Account account) {
        Account accountUpdate = accountRepository.findOne(account.getId());
        if (accountUpdate == null) {
            LOGGER.debug("Part with id " + account.getId() + " not found.");
            throw new AccountNotFoundException();
        }

        accountUpdate.setAccountType(account.getAccountType());
        accountUpdate.setLogin(account.getLogin());
        accountUpdate.setPassword(account.getPassword());
    }

    public void delete(Long id) {
        Account account = accountRepository.findOne(id);
        if (account == null) {
            LOGGER.debug("Part with id " + account.getId() + " not found.");
            throw new AccountNotFoundException();
        }

        accountRepository.delete(account);
    }
}
