package pl.sdacademy.warehouse.service.part.query;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.sdacademy.warehouse.domain.entity.Part;
import pl.sdacademy.warehouse.repository.PartRepository;
import pl.sdacademy.warehouse.service.part.exception.PartNotFoundException;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional(readOnly = true)
public class PartQueryService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PartQueryService.class);

    private final PartRepository partRepository;

    @Autowired
    public PartQueryService (PartRepository partRepository) {
       this.partRepository=partRepository;
    }

    public List<Part> findAll (){
        return partRepository.findAll();
    }

    public Part findById (Long id){
        Part part = partRepository.findOne(id);
        if(part==null){
        LOGGER.debug("Part with id " + id + " not found.");
            throw new PartNotFoundException();
        }

        return part;
    }

    public List<Part> findByNameAndSymbol(String name) {
        List <Part> partList = partRepository.findAll();
        List <Part> parts = new ArrayList<>();
        for(Part part: partList){
            if(part.getPartDefinition().getName().contains(name) || part.getPartDefinition().getSymbol().contains(name)){
                parts.add(part);
            }
        }
        return parts;
    }
}
