package pl.sdacademy.warehouse.service.partDefinition.query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.sdacademy.warehouse.domain.entity.PartDefinition;
import pl.sdacademy.warehouse.domain.entity.Product;
import pl.sdacademy.warehouse.repository.PartDefinitionRepository;
import pl.sdacademy.warehouse.service.part.query.PartQueryService;
import pl.sdacademy.warehouse.service.partDefinition.exception.PartDefinitionNotFoundException;
import pl.sdacademy.warehouse.service.product.exception.ProductNotFoundException;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class PartDefinitionQueryService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PartQueryService.class);

    private final PartDefinitionRepository partDefinitionRepository;

    @Autowired
    public PartDefinitionQueryService (PartDefinitionRepository partDefinitionRepository){
        this.partDefinitionRepository = partDefinitionRepository;
    }

    public List<PartDefinition> findAll (){
        return partDefinitionRepository.findAll();
    }

    public PartDefinition findById (Long id){
        PartDefinition partDefinition = partDefinitionRepository.findOne(id);
        if(partDefinition==null){
            LOGGER.debug("PartDefinition with id " + id + " not found.");
            throw new PartDefinitionNotFoundException();
        }

        return partDefinition;
    }
}
