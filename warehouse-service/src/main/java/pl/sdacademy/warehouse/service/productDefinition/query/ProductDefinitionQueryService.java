package pl.sdacademy.warehouse.service.productDefinition.query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.sdacademy.warehouse.domain.entity.ProductDefinition;
import pl.sdacademy.warehouse.repository.ProductDefinitionRepository;
import pl.sdacademy.warehouse.service.productDefinition.exception.ProductDefinitionNotFoundException;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class ProductDefinitionQueryService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ProductDefinitionQueryService.class);

    private final ProductDefinitionRepository productDefinitionRepository;

    @Autowired
    public ProductDefinitionQueryService(ProductDefinitionRepository productDefinitionRepository) {
        this.productDefinitionRepository = productDefinitionRepository;
    }

    public List<ProductDefinition> findAll() {
        return productDefinitionRepository.findAll();
    }

    public ProductDefinition findById(Long id) {
        ProductDefinition productDefinition = productDefinitionRepository.findOne(id);
        if (productDefinition == null) {
            LOGGER.debug("Part with id " + id + " not found.");
            throw new ProductDefinitionNotFoundException();
        }

        return productDefinition;
    }
}
