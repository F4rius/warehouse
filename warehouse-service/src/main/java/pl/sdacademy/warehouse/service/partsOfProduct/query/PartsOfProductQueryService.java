package pl.sdacademy.warehouse.service.partsOfProduct.query;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.sdacademy.warehouse.service.part.query.PartQueryService;

@Service
@Transactional(readOnly = true)
public class PartsOfProductQueryService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PartQueryService.class);

}
