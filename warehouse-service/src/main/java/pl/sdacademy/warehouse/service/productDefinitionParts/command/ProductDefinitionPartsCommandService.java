package pl.sdacademy.warehouse.service.productDefinitionParts.command;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.sdacademy.warehouse.domain.entity.ProductDefinitionParts;
import pl.sdacademy.warehouse.domain.entity.ProductDefinitionPartsId;
import pl.sdacademy.warehouse.repository.ProductDefinitionPartsRepository;
import pl.sdacademy.warehouse.service.partDefinition.query.PartDefinitionQueryService;
import pl.sdacademy.warehouse.service.productDefinition.query.ProductDefinitionQueryService;
import pl.sdacademy.warehouse.service.productDefinitionParts.exception.ProductDefinitionPartsNotFoundException;
import pl.sdacademy.warehouse.service.productDefinitionParts.query.ProductDefinitionPartsQueryService;

@Service
@Transactional
public class ProductDefinitionPartsCommandService {
    private final Logger LOGGER = LoggerFactory.getLogger(ProductDefinitionPartsCommandService.class);

    private ProductDefinitionPartsRepository productDefinitionPartsRepository;
    private ProductDefinitionPartsQueryService productDefinitionPartsQueryService;
    private PartDefinitionQueryService partDefinitionQueryService;
    private ProductDefinitionQueryService productDefinitionQueryService;

    @Autowired
    public ProductDefinitionPartsCommandService(ProductDefinitionPartsRepository productDefinitionPartsRepository, ProductDefinitionPartsQueryService productDefinitionPartsQueryService, PartDefinitionQueryService partDefinitionQueryService, ProductDefinitionQueryService productDefinitionQueryService) {
        this.productDefinitionPartsRepository = productDefinitionPartsRepository;
        this.productDefinitionPartsQueryService = productDefinitionPartsQueryService;
        this.partDefinitionQueryService = partDefinitionQueryService;
        this.productDefinitionQueryService = productDefinitionQueryService;
    }

    public Long saveProductDefinitionParts(ProductDefinitionParts productDefinitionParts) {
//        ProductDefinitionPartsId productDefinitionPartsId = new ProductDefinitionPartsId();
//        productDefinitionPartsId.setPartDefinition(partDefinitionQueryService.findById(
//                productDefinitionParts
//                        .getProductDefinitionPartsId()
//                        .getPartDefinition()
//                        .getId()));
//
//        productDefinitionPartsId.setProductDefinition(productDefinitionQueryService.findById(
//                productDefinitionParts
//                        .getProductDefinitionPartsId()
//                        .getProductDefinition()
//                        .getId()));
//
//        productDefinitionParts.setProductDefinitionPartsId(productDefinitionPartsId);
        productDefinitionPartsRepository.save(productDefinitionParts);

        return productDefinitionParts.getId();
    }

    public void updateProductDefinitionParts(ProductDefinitionParts productDefinitionParts) {
//        ProductDefinitionPartsId productDefinitionPartsId = new ProductDefinitionPartsId();
//        productDefinitionPartsId.setPartDefinition(partDefinitionQueryService.findById(idPart));
//        productDefinitionPartsId.setProductDefinition(productDefinitionQueryService.findById(idProduct));

        ProductDefinitionParts productDefinitionPartsDB = productDefinitionPartsRepository.findOne(productDefinitionParts.getId());
        productDefinitionPartsDB.setQuantity(productDefinitionParts.getQuantity());
//        productDefinitionPartsDB.setId(productDefinitionParts.getId());

//        productDefinitionPartsUpdate.setProductDefinitionPartsId(productDefinitionPartsId);
//        productDefinitionPartsUpdate.setId(productDefinitionParts.getId());
//
//
//        ProductDefinitionParts productDefPartsUpdate = productDefinitionPartsRepository.findOne(productDefinitionParts.getId());
//        if (productDefPartsUpdate == null) {
//            LOGGER.debug("Part with id " + productDefinitionParts.getId() + " not found.");
//            throw new ProductDefinitionPartsNotFoundException();
//        }
//
//        productDefPartsUpdate.setProductDefinitionPartsId(productDefinitionParts.getProductDefinitionPartsId());
//        productDefPartsUpdate.setQuantity(productDefinitionParts.getQuantity());

    }

    public void delete(Long id) {
        ProductDefinitionParts productDefinitionParts = productDefinitionPartsRepository.findOne(id);
        if (productDefinitionParts == null) {
            LOGGER.debug("Part with id " + productDefinitionParts.getId() + " not found.");
            throw new ProductDefinitionPartsNotFoundException();
        }
        productDefinitionPartsRepository.delete(productDefinitionParts);
    }//TODO do przerobienia

}

