package pl.sdacademy.warehouse.service.product.query;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.sdacademy.warehouse.domain.entity.Product;
import pl.sdacademy.warehouse.repository.ProductRepository;
import pl.sdacademy.warehouse.service.part.query.PartQueryService;
import pl.sdacademy.warehouse.service.product.exception.ProductNotFoundException;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class ProductQueryService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PartQueryService.class);

    private final ProductRepository productRepository;

    @Autowired
    public ProductQueryService (ProductRepository productRepository){
        this.productRepository = productRepository;
    }

    public List<Product> findAll (){
        return productRepository.findAll();
    }

    public Product findById (Long id){
        Product product = productRepository.findOne(id);
        if(product==null){
            LOGGER.debug("Part with id " + id + " not found.");
            throw new ProductNotFoundException();
        }

        return product;
    }
}
