package pl.sdacademy.warehouse.service.part.command;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.sdacademy.warehouse.domain.entity.Part;
import pl.sdacademy.warehouse.repository.PartRepository;
import pl.sdacademy.warehouse.service.part.exception.PartNotFoundException;


@Service
@Transactional
public class PartCommandService {

    private final Logger LOGGER = LoggerFactory.getLogger(PartCommandService.class);

    private PartRepository partRepository;

    @Autowired
    public PartCommandService (PartRepository partRepository){
        this.partRepository = partRepository;
    }

    public Long savePart (Part part){
        partRepository.save(part);

        return part.getId();
    }

    public void update (Part part){
        Part partUpdate = partRepository.findOne(part.getId());
        if(partUpdate==null){
            LOGGER.debug("Part with id " + part.getId() + " not found.");
            throw new PartNotFoundException();
        }

        double newStringQuantity = partUpdate.getQuantity()+(part.getVersion()* part.getQuantity());

        partUpdate.setQuantity(newStringQuantity);
    }

}
