<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>

<!DOCTYPE html>
<html lang="en">

<jsp:include page="common/header.jsp"/>

<body>
<div class="container">

    <jsp:include page="common/menu.jsp"/>

    <div class="container">

        <div class="table-responsive">
            <table class="table table-hover table-striped">
                <thead>
                <tr>
                    <th scope="col"><spring:message code="part.partName"/></th>
                    <th scope="col"><spring:message code="part.symbol"/></th>
                    <th scope="col"><spring:message code="part.unit"/></th>
                    <th></th>
                    <th scope="col" class="col-sm-4"><spring:message code="part.actions"/></th>
                </tr>
                </thead>
                <tbody>

                <c:forEach items="${productDefinitionParts}" var="productDefinitionPart">
                    <%--<spring:url var="saveAction"--%>
                                <%--value="/productDefinition/addPart/${productDefinitionPart.productDefinitionPartsId.productDefinition.id}/${productDefinitionPart.productDefinitionPartsId.partDefinition.id}"/>--%>
                    <spring:url var="saveAction"
                                value="/productDefinition/addPart"/>
                    <form:form method="post" action="${saveAction}">

                        <tr>
                            <td>${productDefinitionPart.productDefinitionPartsId.partDefinition.name}</td>
                            <td>${productDefinitionPart.productDefinitionPartsId.partDefinition.symbol}</td>
                            <td>${productDefinitionPart.productDefinitionPartsId.partDefinition.unit}</td>
                            <td>
                                <input type="hidden" name="id" value="${productDefinitionPart.id}"/>
                                <input type="hidden" name="productDefinitionPartsId.productDefinition.id" value="${productDefinitionPart.productDefinitionPartsId.productDefinition.id}"/>
                                <input type="hidden" name="productDefinitionPartsId.partDefinition.id" value="${productDefinitionPart.productDefinitionPartsId.partDefinition.id}"/>
                            </td>
                            <td>
                                <input type="text" name="quantity" placeholder="${productDefinitionPart.quantity}"/>
                                <div class="btn-group">
                                    <button class="btn btn-primary" type="submit">
                                        <spring:message code="part.addPart"/>
                                    </button>
                                </div>
                            </td>
                        </tr>
                    </form:form>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>
<jsp:include page="common/footer.jsp"/>
</body>
</html>