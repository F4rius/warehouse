<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>

<!DOCTYPE html>
<html lang="en">

<jsp:include page="common/header.jsp"/>

<body>
<div class="container">

    <jsp:include page="common/menu.jsp"/>

        <div class="container">
        <spring:url var="findPartUrl" value="/part/find/"/>
        <form:form modelAttribute="partDefinition" action="${findPartUrl}" method="get">
            <br><b><spring:message code="part.search"/></b><br>
            <input type="text" name="name" path="name"/>
            <%--<input type="hidden" name="name" value="${part.partDefinition.name}"/>--%>
                <div class="btn-group">
                    <button class="btn btn-primary" type="submit">
                        <spring:message code="part.search"/>
                    </button>
                </div>
            </form:form>

    <div class="container">

        <div class="table-responsive">
            <table class="table table-hover table-striped">
                <thead>
                <tr>
                    <th scope="col"><spring:message code="part.partName"/></th>
                    <th scope="col"><spring:message code="part.symbol"/></th>
                    <th scope="col"><spring:message code="part.unit"/></th>
                    <th scope="col"><spring:message code="part.quantity"/></th>
                    <th scope="col"/>
                    <th scope="col" class="col-sm-4"><spring:message code="part.actions"/></th>
                </tr>
                </thead>
                <tbody>
                <c:choose>
                    <c:when test="${!empty parts}">
                        <c:forEach items="${parts}" var="part">
                    <spring:url var="saveAction"
                                value="/part/edit"/>
                    <form:form method="post" modelAttribute="part" action="${saveAction}">
                            <tr>
                                <td>${part.partDefinition.name}</td>
                                <td>${part.partDefinition.symbol}</td>
                                <td>${part.partDefinition.unit}</td>
                                <td>${part.quantity}</td>
                                <td><input type="hidden" name="id" value="${part.id}"/>
                                </td>
                                <td>
                                    <input type="text" name="quantity"/>
                                    <div class="btn-group">
                                        <button class="btn btn-primary" type="submit" name="version" value="1">
                                            Przyjmij
                                        </button>
                                        <button class="btn btn-primary" type="submit" name="version" value="-1">
                                            Wydaj
                                        </button>
                                    </div>
                                </td>
                            </tr>
                         </form:form>
                        </c:forEach>
                    </c:when>
                    <c:otherwise>
                        <tr>
                            <td>
                                <spring:message code="part.listIsEmpty"/>
                            </td>
                        </tr>
                    </c:otherwise>
                </c:choose>
                <%--<tr>--%>
                    <%--<spring:url var="saveAction" value="/part/save"/>--%>

                    <%--<form:form method="post" modelAttribute="part" action="${saveAction}" class="form-horizontal">--%>

                        <%--<form:hidden path="id"/>--%>

                        <%--<spring:bind path="partName">--%>
                            <%--<td class="form-group ${status.error ? 'has-error' : ''}">--%>
                                <%--<form:label path="partName" class="col-sm-2 control-label">--%>
                                <%--</form:label>--%>
                                <%--<div class="col-sm-10">--%>
                                    <%--<form:input path="partName" class="form-control"/>--%>
                                    <%--<form:errors path="partName" class="control-label"/>--%>
                                <%--</div>--%>
                            <%--</td>--%>
                        <%--</spring:bind>--%>

                        <%--<spring:bind path="symbol">--%>
                            <%--<td class="form-group ${status.error ? 'has-error' : ''}">--%>
                                <%--<form:label path="symbol" class="col-sm-2 control-label">--%>
                                <%--</form:label>--%>
                                <%--<div class="col-sm-10">--%>
                                    <%--<form:input path="symbol" class="form-control"/>--%>
                                    <%--<form:errors path="symbol" class="control-label"/>--%>
                                <%--</div>--%>
                            <%--</td>--%>
                        <%--</spring:bind>--%>

                        <%--<spring:bind path="quantity">--%>
                            <%--<td class="form-group ${status.error ? 'has-error' : ''}">--%>
                                <%--<form:label path="quantity" class="col-sm-2 control-label">--%>
                                <%--</form:label>--%>
                                <%--<div class="col-sm-10">--%>
                                    <%--<form:input path="quantity" class="form-control"/>--%>
                                    <%--<form:errors path="quantity" class="control-label"/>--%>
                                <%--</div>--%>
                            <%--</td>--%>
                        <%--</spring:bind>--%>

                        <%--<spring:bind path="unit">--%>
                            <%--<td class="form-group ${status.error ? 'has-error' : ''}">--%>
                                <%--<form:label path="unit" class="col-sm-2 control-label">--%>
                                <%--</form:label>--%>
                                <%--<div class="col-sm-10">--%>
                                    <%--<form:input path="unit" class="form-control"/>--%>
                                    <%--<form:errors path="unit" class="control-label"/>--%>
                                <%--</div>--%>
                            <%--</td>--%>
                        <%--</spring:bind>--%>

                        <%--<td>--%>
                            <%--<button class="btn btn-primary" type="submit">--%>
                                <%--<spring:message code="part.addPart"/>--%>
                            <%--</button>--%>
                        <%--</td>--%>

                    <%--</form:form>--%>
                <%--</tr>--%>
                <%--</tbody>--%>
            <%--</table>--%>
        <%--</div>--%>
    <%--</div>--%>

    <%--<div class="container">--%>


    <%--</div>--%>

<%--</div>--%>
<jsp:include page="common/footer.jsp"/>
</body>
</html>
