<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>

<!DOCTYPE html>
<html lang="en">

<jsp:include page="common/header.jsp"/>

<body>
<div class="container">
    <div class="row">
        <div class="col-sm-4"></div>
        <div class="col-sm-4">
            <p>
            <h2 class="text-center"><spring:message code="login.welcome"/></h2>
            </p>
        </div>
        <div class="col-sm-4"></div>
    </div>

    <div class="row">
        <div class="col-sm-4"></div>
        <div class="col-sm-4">
            <spring:url var="login" value="perform_login"/>

            <form:form method="post" modelAttribute="login" action="${login}" class="form-horizontal">

                <spring:bind path="username">
                    <div class="form-group ${status.error ? 'has-error' : ''}">
                        <form:label path="username" class="col-sm-12 control-label">
                        </form:label>

                        <spring:message var="usernamePlaceholder" code="login.username"/>
                        <form:input path="username" class="form-control col-sm-8"
                                    placeholder="${usernamePlaceholder}"/>
                        <form:errors path="username" class="control-label"/>
                    </div>
                </spring:bind>

                <spring:bind path="password">
                    <div class="form-group ${status.error ? 'has-error' : ''}">
                        <form:label path="password" class="col-sm-12 control-label">

                        </form:label>

                        <spring:message var="passwordPlaceholder" code="login.password"/>
                        <form:input path="password" type="password" class="form-control col-sm-8"
                                    placeholder="${passwordPlaceholder}"/>
                        <form:errors path="password" class="control-label"/>

                    </div>
                </spring:bind>

                <div class="row">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-6" btn>
                        <button class="btn btn-primary btn-block" type="submit">
                            <spring:message code="login.submit"/>
                        </button>
                    </div>
                    <div class="col-sm-3"></div>
                </div>
            </form:form>

        </div>
        <div class="col-sm-4"></div>

    </div>
</div>
<jsp:include page="common/footer.jsp"/>
</body>
</html>


<%--<h2><spring:message code="login.title"/></h2>--%>
<%--narazie tak bo nie ma zmieniania jezykow--%>


<%--<form:form method="post" modelAttribute="login" action="${login}">--%>
<%--<table>--%>
<%--<tr>--%>
<%--<td>User</td>--%>
<%--<td>--%>
<%--<input type='text' name='username' value=''>--%>
<%--</td>--%>
<%--</tr>--%>

<%--<tr>--%>

<%--<td>Password</td>--%>
<%--<td>--%>
<%--<input type='password' name='password'/>--%>
<%--</td>--%>
<%--</tr>--%>

<%--<tr>--%>
<%--<td colspan='2'>--%>
<%--&lt;%&ndash;<spring:message code="login.submit.label" var="labelSubmit"/>&ndash;%&gt;--%>
<%--&lt;%&ndash;narazie tak bo nie ma zmieniania jezykow&ndash;%&gt;--%>

<%--&lt;%&ndash;<input name="submit" type="submit" value="${labelSubmit}"/>&ndash;%&gt;--%>
<%--<input name="submit" type="submit" value="Login"/>--%>
<%--</td>--%>
<%--</tr>--%>

<%--</table>--%>
<%--</form:form>--%>
