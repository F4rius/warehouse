<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>

<!DOCTYPE html>
<html lang="en">

<jsp:include page="common/header.jsp"/>

<body>

<%--<c:url var="logoutUrl" value="/perform_logout"/>--%>
<%--&lt;%&ndash;<spring:message code="logout.submit.label" var="labelSubmit"/>&ndash;%&gt;--%>
<%--&lt;%&ndash;// nie ma zmieniania jezykow wiec narazie tak&ndash;%&gt;--%>

<%--<form:form method="post" action="${logoutUrl}">--%>
<%--<input type="submit"--%>
<%--&lt;%&ndash;value="${labelSubmit}"/>&ndash;%&gt;--%>
<%--&lt;%&ndash;// nie ma zmieniania jezykow wiec narazie tak&ndash;%&gt;--%>
<%--value="Logout"/>--%>
<%--</form:form>--%>

<div class="container">

    <jsp:include page="common/menu.jsp"/>

    <spring:url var="partUrl" value="/part"/>
    <spring:url var="productUrl" value="/product"/>

    <div class="container">
        <div class="list-group">
            <a href="${partUrl}" class="list-group-item list-group-item-action">Magazyn części</a>
            <a href="${productUrl}" class="list-group-item list-group-item-action">Produkty</a>
            <a href="#" class="list-group-item list-group-item-action disabled">Lina produkcyjna</a>
            <a href="#" class="list-group-item list-group-item-action disabled">Braki</a>
        </div>
    </div>
</div>
<jsp:include page="common/footer.jsp"/>
</body>
</html>
