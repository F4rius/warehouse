package pl.sdacademy.warehouse.web.dto;

import pl.sdacademy.warehouse.domain.entity.PartDefinition;

public class ProductDefPartsDTO {

    private ProductDefinitionDTO productDefinition;

    private PartDefinition partDefinition;

    private double count;

    public ProductDefinitionDTO getProductDefinition() {
        return productDefinition;
    }

    public void setProductDefinition(ProductDefinitionDTO productDefinition) {
        this.productDefinition = productDefinition;
    }

    public PartDefinition getPartDefinition() {
        return partDefinition;
    }

    public void setPartDefinition(PartDefinition partDefinition) {
        this.partDefinition = partDefinition;
    }

    public double getCount() {
        return count;
    }

    public void setCount(double count) {
        this.count = count;
    }
}
