package pl.sdacademy.warehouse.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import pl.sdacademy.warehouse.domain.entity.Part;
import pl.sdacademy.warehouse.domain.entity.PartDefinition;
import pl.sdacademy.warehouse.service.part.command.PartCommandService;
import pl.sdacademy.warehouse.service.part.query.PartQueryService;


@Controller
public class MainController {
    private static final Logger LOGGER = LoggerFactory.getLogger(MainController.class);

    private final PartCommandService partCommandService;
    private final PartQueryService partQueryService;

    @Autowired
    public MainController(PartQueryService partQueryService, PartCommandService partCommandService) {
        this.partCommandService = partCommandService;
        this.partQueryService = partQueryService;
    }

    @RequestMapping(value = "/main", method = RequestMethod.GET)
    public String index() {
        return "index";
    }

    @RequestMapping(value = "/part", method = RequestMethod.GET)
    public String showAllParts(@ModelAttribute("part") Part part, Model model) {
        LOGGER.debug("is executed!");
        model.addAttribute("parts", partQueryService.findAll());
        return "part";
    }

    @RequestMapping(value = "/part/edit")
    public String editPart(@ModelAttribute("part") Part part) {
        LOGGER.debug("is executed!");
        partCommandService.update(part);
        return "redirect:/part";
    }

    @RequestMapping(value = "/part/find/")
    public String showFindParts(@ModelAttribute("partDefinition") PartDefinition partDefinition, Model model){
        model.addAttribute("parts", partQueryService.findByNameAndSymbol(partDefinition.getName()));
        return "part";
    }

}
