package pl.sdacademy.warehouse.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import pl.sdacademy.warehouse.domain.entity.Product;
import pl.sdacademy.warehouse.service.product.command.ProductCommandService;
import pl.sdacademy.warehouse.service.product.query.ProductQueryService;

import javax.validation.Valid;

@Controller
public class ProductController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProductController.class);

    private final ProductQueryService productQueryService;
    private final ProductCommandService productCommandService;

    @Autowired
    public ProductController (ProductCommandService productCommandService, ProductQueryService productQueryService){
        this.productCommandService = productCommandService;
        this.productQueryService = productQueryService;
    }

    @RequestMapping(value = "/product", method = RequestMethod.GET)
    public String showAllProducts (@ModelAttribute("product") Product product, Model model){
        LOGGER.debug("is executed!");
        model.addAttribute("products", productQueryService.findAll());
        model.addAttribute("product", product);
        return "product";
    }

    @RequestMapping(value = "/product/save", method = RequestMethod.POST)
    public String saveProducts (@Valid @ModelAttribute("product") Product product,
                                BindingResult bindingResult, RedirectAttributes redirectAttributes){
        LOGGER.debug("is executed!");
        if(bindingResult.hasErrors()){
            redirectAttributes.addFlashAttribute(BindingResult.MODEL_KEY_PREFIX + "product", bindingResult);
            redirectAttributes.addFlashAttribute("flash" + "product", product);

            return "redirect:/product";
        }

        if (product.getId() == null) {
            productCommandService.saveProduct(product);
        } else {
            productCommandService.update(product);
        }

        return "redirect:/product";
    }

    @RequestMapping(value = "/product/edit/{id}")
    public String getProduct (@PathVariable("id") Long id, Model model){
        LOGGER.debug("is executed!");
        model.addAttribute("products", productQueryService.findAll());
        model.addAttribute("product", productQueryService.findById(id));

        return "product";
    }

    @RequestMapping(value = "/product/delete/{id}")
    public String deleteProduct (@PathVariable("id") Long id){
        LOGGER.debug("is executed!");
        productCommandService.delete(id);
        return "redirect:/product";
    }

    @RequestMapping(value = "/product/showParts/{id}")
    public String showParts (@PathVariable("id") Long id, Model model){
//        model.addAttribute("partsOfProduct",partsOfProductQueryService.findAll());
        return "showPartsOfProduct";
    }
}
