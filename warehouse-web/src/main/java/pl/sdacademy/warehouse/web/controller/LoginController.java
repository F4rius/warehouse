package pl.sdacademy.warehouse.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import pl.sdacademy.warehouse.web.dto.LoginDTO;

import java.util.Locale;

@Controller
public class LoginController {

    private static final Logger LOGGER = LoggerFactory.getLogger(LoginController.class);

    private static final String MODEL_LOGIN = "login";

    private static final String VIEW_LOGIN = "login";

    private final MessageSource messageSource;

    @Autowired
    public LoginController(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public ModelAndView login(@RequestParam(required = false) String error,
                              @RequestParam(required = false) String logout,
                              @ModelAttribute(MODEL_LOGIN) LoginDTO login,
                              Locale locale) {
        LOGGER.debug("is executed!");

        ModelAndView model = new ModelAndView();
        if (error != null) {
            model.addObject("error",
                    messageSource.getMessage("login.message.error", new String[]{}, locale));
        }

        if (logout != null) {
            model.addObject("msg",
                    messageSource.getMessage("logout.message.success", new String[]{}, locale));
        }
        model.addObject(MODEL_LOGIN, login);
        model.setViewName(VIEW_LOGIN);

        return model;
    }
}
