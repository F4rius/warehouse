package pl.sdacademy.warehouse.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import pl.sdacademy.warehouse.domain.entity.Account;
import pl.sdacademy.warehouse.domain.entity.utils.AccountType;
import pl.sdacademy.warehouse.service.account.command.AccountCommandService;
import pl.sdacademy.warehouse.service.account.query.AccountQueryService;

import javax.validation.Valid;
import java.util.*;

@Controller
public class AccountController {

    private static final Logger LOGGER = LoggerFactory.getLogger(AccountController.class);

    private final AccountCommandService accountCommandService;
    private final AccountQueryService accountQueryService;

    @Autowired
    public AccountController(AccountCommandService accountCommandService, AccountQueryService accountQueryService) {
        this.accountCommandService = accountCommandService;
        this.accountQueryService = accountQueryService;
    }

    @RequestMapping(value = "/account", method = RequestMethod.GET)
    public String showAllAccounts(@ModelAttribute("account")Account account, Model model) {
        LOGGER.debug("is executed!");
        model.addAttribute("accounts", accountQueryService.findAll());
        model.addAttribute("account", account);

//        List<Enum> accountTypes = Arrays.asList(AccountType.values());

        Map<AccountType,String> accountType = generateAccountList();
        model.addAttribute("accountTypeList", accountType);

        return "account";
    }

    @RequestMapping(value = "/account/save", method = RequestMethod.POST)
    public String saveAccount(@Valid @ModelAttribute("account") Account account,
                           BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        LOGGER.debug("is executed!");
        if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute(BindingResult.MODEL_KEY_PREFIX + "account", bindingResult);
            redirectAttributes.addFlashAttribute("flash" + "account", account);

            return "redirect:/account";
        }

        if (account.getId() == null) {
            accountCommandService.saveAccount(account);
        } else {
            accountCommandService.update(account);
        }

        return "redirect:/account";
    }

    @RequestMapping(value = "/account/edit/{id}")
    public String editAccount(@PathVariable("id") Long id, Model model) {
        LOGGER.debug("is executed!");
        model.addAttribute("accounts", accountQueryService.findAll());
        model.addAttribute("account", accountQueryService.findById(id));

        Map<AccountType,String> accountType = generateAccountList();
        model.addAttribute("accountTypeList", accountType);

        return "account";
    }

    @RequestMapping(value = "/account/delete/{id}")
    public String deleteAccount(@PathVariable("id") Long id) {
        LOGGER.debug("is executed!");
        accountCommandService.delete(id);
        return "redirect:/account";
    }

    private Map<AccountType,String> generateAccountList(){
        Map<AccountType,String> accountType = new LinkedHashMap<>();
        accountType.put(AccountType.ADMIN,"Admin");
        accountType.put(AccountType.WAREHOUSEMAN,"Magazynier");
        accountType.put(AccountType.ENGINEER,"Inżynier");
        accountType.put(AccountType.OTHER,"Inny");
        return accountType;
    }


}
