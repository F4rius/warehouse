package pl.sdacademy.warehouse.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import pl.sdacademy.warehouse.domain.entity.PartDefinition;
import pl.sdacademy.warehouse.service.partDefinition.command.PartDefinitionCommandService;
import pl.sdacademy.warehouse.service.partDefinition.query.PartDefinitionQueryService;

import javax.validation.Valid;

@Controller
public class PartDefinitionController {

    private static final Logger LOGGER = LoggerFactory.getLogger(PartDefinitionController.class);

    private final PartDefinitionQueryService partDefinitionQueryService;
    private final PartDefinitionCommandService partDefinitionCommandService;

    @Autowired
    public  PartDefinitionController (PartDefinitionCommandService partDefinitionCommandService, PartDefinitionQueryService partDefinitionQueryService){
        this.partDefinitionCommandService = partDefinitionCommandService;
        this.partDefinitionQueryService = partDefinitionQueryService;
    }

    @RequestMapping(value = "/partDefinition", method = RequestMethod.GET)
    public String showAllPartsDef(@ModelAttribute("part")PartDefinition partDefinition, Model model) {
        LOGGER.debug("is executed!");
        model.addAttribute("partsDefinition", partDefinitionQueryService.findAll());
        model.addAttribute("partDefinition", partDefinition);
        return "partDefinition";
    }

    @RequestMapping(value = "/partDefinition/save", method = RequestMethod.POST)
    public String savePartDef(@Valid @ModelAttribute("partDefinition") PartDefinition partDefinition,
                           BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        LOGGER.debug("is executed!");
        if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute(BindingResult.MODEL_KEY_PREFIX + "partDefinition", bindingResult);
            redirectAttributes.addFlashAttribute("flash" + "partDefinition", partDefinition);

            return "redirect:/partDefinition";
        }

        if (partDefinition.getId() == null) {
            partDefinitionCommandService.savePart(partDefinition);
        } else {
            partDefinitionCommandService.update(partDefinition);
        }

        return "redirect:/partDefinition";
    }

    @RequestMapping(value = "/partDefinition/edit/{id}")
    public String getPartDef(@PathVariable("id") Long id, Model model) {
        LOGGER.debug("is executed!");
        model.addAttribute("partsDefinition", partDefinitionQueryService.findAll());
        model.addAttribute("partDefinition", partDefinitionQueryService.findById(id));

        return "partDefinition";
    }

    @RequestMapping(value = "/partDefinition/delete/{id}")
    public String deletePart(@PathVariable("id") Long id) {
        LOGGER.debug("is executed!");
        partDefinitionCommandService.delete(id);
        return "redirect:/partDefinition";
    }


}
